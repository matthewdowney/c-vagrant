touch install
echo yum install -y centos-release-scl >> install
echo yum install -y centos-release-scl-rh >> install
echo yum-config-manager --enable rhel-server-rhscl-7-rpms >> install
echo yum install -y devtoolset-3 >> install
echo yum install -y devtoolset-3-gcc >> install
echo yum install -y devtoolset-3-gcc-c++ >> install
echo yum install -y vim >> install
echo yum install -y xorg-x11-xauth xorg-x11-fonts-* xorg-x11-utils >> install
chmod +x install

touch start
echo cd /sh/code >> start
echo source enable.sh >> start
chmod +x start

echo "[Provisioner] Setup complete. Run sudo ./install once and then ./start afterward to start developing. Why is it so complex? Shouldn't it be automatic?? Idk I suck at writing provision scripts."
